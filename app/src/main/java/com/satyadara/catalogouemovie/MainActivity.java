package com.satyadara.catalogouemovie;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.satyadara.catalogouemovie.adapter.MovieAdapter;
import com.satyadara.catalogouemovie.model.Movie;
import com.satyadara.catalogouemovie.remote.MovieService;
import com.satyadara.catalogouemovie.remote.RetrofitClient;
import com.satyadara.catalogouemovie.response.MovieResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.listMovies)
    RecyclerView recyclerView;
    List<Movie> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<>();

        clickSearch();
    }

    private void clickSearch() {
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edSearch.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Silahkan masukan kata kunci !", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Silahkan tunggu . . .", Toast.LENGTH_LONG).show();
                    recyclerView.setAdapter(null);
                    Retrofit retrofit = RetrofitClient.getClient();
                    MovieService movieService = retrofit.create(MovieService.class);
                    Call<MovieResponse> call = movieService.searchMovie(
                            "0553ea4b7169cee1993e121828c11830",
                            "en-US",
                            edSearch.getText().toString()
                    );
                    call.enqueue(new Callback<MovieResponse>() {
                        @Override
                        public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                            list = response.body().getResults();
                            MovieAdapter movieAdapter = new MovieAdapter(MainActivity.this, list);
                            recyclerView.setAdapter(movieAdapter);
                        }

                        @Override
                        public void onFailure(Call<MovieResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
}
